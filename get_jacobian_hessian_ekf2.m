function [ f_func_g,f_jacobian,f_hessian ] = get_jacobian_hessian_ekf2(ellipse_par,movement,hc1,hc2)

syms m1 m2 e1 e2 l1 l2 a1 a2 b1 b2 h1 h2 v1 v2 s1 s2 w

if strcmp(ellipse_par,'el')
    if strcmp(movement,'static')
        X = [m1;m2; e1;e2;l1;l2;h1;h2;v1;v2];
        
    elseif strcmp(movement,'NCV')
        X = [m1;m2; e1;e2;l1;l2;s1;s2;h1;h2;v1;v2];
        
    else
        error('unknown dynmaic model');
    end
    func_g =[         e1^2 + e2^2;...
        (m1 + h1*l1*e1 - h2*l2*e2 + v1);...
        (m2 + h1*l1*e2 + h2*l2*e1 + v2);...
        (m1 + h1*l1*e1 - h2*l2*e2 + v1)^2;...
        (m2 + h1*l1*e2 + h2*l2*e1 + v2)^2;...
        (m1 + h1*l1*e1 - h2*l2*e2 + v1)*(m2 + h1*l1*e2 + h2*l2*e1 + v2)];
    
elseif strcmp(ellipse_par,'ab')
    func_g =  [a1*b1+a2*b2;...
                (m1 + h1*a1 + h2*b1 + v1);...
        (m2 + h1*a2 + h2*b2 + v2);...
        (m1 + h1*a1 + h2*b1 + v1)^2;...
        (m2 + h1*a2 + h2*b2 + v2)^2;...
        (m1 + h1*a1 + h2*b1 + v1)*(m2 + h1*a2 + h2*b2 + v2)];
    
    if strcmp(movement,'static')
        X = [m1;m2; a1;a2;b1;b2;h1;h2;v1;v2];
        
    elseif strcmp(movement,'NCV')
        X = [m1;m2; a1;a2;b1;b2;s1;s2;h1;h2;v1;v2];
        
    else
        error('unknown dynmaic model');
    end
    
elseif strcmp(ellipse_par,'alpha')
    func_g =[(m1 + h1*l1*cos(w) - h2*l2*sin(w) + v1);...
        (m2 + h1*l1*sin(w) + h2*l2*cos(w) + v2);...
        (m1 + h1*l1*cos(w) - h2*l2*sin(w) + v1)^2;...
        (m2 + h1*l1*sin(w) + h2*l2*cos(w) + v2)^2;...
        (m1 + h1*l1*cos(w) - h2*l2*sin(w) + v1)*(m2 + h1*l1*sin(w) + h2*l2*cos(w) + v2)];
    
    if strcmp(movement,'static')
        X = [m1;m2; w;l1;l2;h1;h2;v1;v2];
        
    elseif strcmp(movement,'NCV')
        X = [m1;m2; w;l1;l2;s1;s2;h1;h2;v1;v2];
        
    else
        error('unknown dynmaic model');
    end
    
else
    error('unknown ellipse parameterization')
end


jacobian_mat = jacobian(func_g,X);



for i = 1:numel(func_g)
    hessian_mat(:,:,i) = hessian(func_g(i),X);
    
end
jacobian_mat = subs(expand(jacobian_mat),[h1^2,h2^2],[hc1,hc2]);
hessian_mat = subs(expand(hessian_mat),[h1^2,h2^2],[hc1, hc2]);

if strcmp(ellipse_par,'el')
f_func_g = matlabFunction(func_g, 'Vars', {[m1, m2, e1,e2, l1, l2, h1, h2, v1, v2]});
f_jacobian = matlabFunction(jacobian_mat, 'Vars', {[m1, m2, e1,e2, l1, l2, h1, h2, v1, v2]});
f_hessian = matlabFunction(hessian_mat, 'Vars', {[m1, m2, e1,e2, l1, l2, h1, h2, v1, v2]});
elseif strcmp(ellipse_par,'alpha')
f_func_g = matlabFunction(func_g, 'Vars', {[m1, m2, w, l1, l2, h1, h2, v1, v2]});
f_jacobian = matlabFunction(jacobian_mat, 'Vars', {[m1, m2, w, l1, l2, h1, h2, v1, v2]});
f_hessian = matlabFunction(hessian_mat, 'Vars', {[m1, m2, w, l1, l2, h1, h2, v1, v2]});
elseif strcmp(ellipse_par,'ab')
  f_func_g = matlabFunction(func_g, 'Vars', {[m1, m2, a1,a2, b1, b2, h1, h2, v1, v2]});
f_jacobian = matlabFunction(jacobian_mat, 'Vars', {[m1, m2, a1,a2, b1, b2, h1, h2, v1, v2]});
f_hessian = matlabFunction(hessian_mat, 'Vars', {[m1, m2, a1,a2, b1, b2, h1, h2, v1, v2]});
end

end

