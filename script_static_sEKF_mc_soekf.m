% close all
% close all
clc
clear all
%  rng default
% load('s.mat')
% rng(s)
gt = [0 0 sqrt(3)/2 2 9]';

% when cv is very small, batch method doesnt work very well
Cv = 0*diag([2 9]);
Ch = diag([1/4,1/4]);

nrMeas = 100;

rotMatrix = @(alpha) [cos(alpha) -sin(alpha); sin(alpha) cos(alpha)];
x0 = [1 1 0 2 12]';
%  x0=gt;
Cx0 = diag([1 1 1 4 3^2]);

x0_ekf = x0;
Cx0_ekf = Cx0;
x0_batch = x0;
Cx0_batch = Cx0;
x0ekf2 = x0;
Cx0ekf2 = Cx0;

alpha = 5;
tau = 200;

T = 10;
const_z = 1/4;
hat_x_RMM = x0(1:2);
hat_X_RMM = get_random_matrix_state(x0(3:5));
Cx_RMM = Cx0(1:2,1:2);

hat_x_RMMLan = x0(1:2);
hat_X_RMMLan = get_random_matrix_state(x0(3:5));
Cx_RMMLan = Cx0(1:2,1:2);

[f_func_g,f_jacobian,f_hessian] = get_jacobian_hessian_ekf2('alpha','static',1/4,1/4);
% [Jp, Hp] = get_jacobian_hessian_ekf;
% h = mvnrnd([0 0],Ch,nrMeas)';

gtRot = rotMatrix(gt(3));
for n = 1:nrMeas
    h(n,:) = -1 + 2.*rand(1,2);
    while norm(h(n,:)) > 1
        h(n,:) = -1 + 2.*rand(1,2);
    end
    y(:,n) = gt(1:2) + gtRot*diag(gt(4:5))*h(n,:)'+mvnrnd([0 0], Cv, 1)';
end

 figure

hold on
box on
axis equal
plotgt=plot_extent_patch(gt);

step = 10;
for i = 1:step:nrMeas
[hat_x_RMM, hat_X_RMM, C_x_RMM, alpha_update]= updateRMM(hat_x_RMM, hat_X_RMM, Cx_RMM, alpha,mean(y,2), ...
    (nrMeas - 1) * cov(y(:,i:i+step-1)'), Cv,nrMeas,eye(2),const_z);

[~, len_RMM,ang_RMM] = get_random_matrix_ellipse(hat_X_RMM);
rmm_par = [hat_x_RMM; ang_RMM;len_RMM];

plotrmm=plot_extent(rmm_par,'-','r',2);

d_RMM(i:i+step-1) = repmat( d_gaussian_wasserstein(gt,rmm_par),1,step);
% 
% [hat_r_batch, Cr_batch,hat_p_batch, Cp_batch ] = updateBatchEKFmc...
%         (eye(2),x0_batch(1:2), Cx0_batch(1:2,1:2), x0_batch(3:5), Cx0_batch(3:5,3:5), y(:,i:i+step-1), Cv, Ch);
%     plotbatchekf=plot_extent([hat_r_batch;hat_p_batch],'-','g',2);
%     x0_batch = [hat_r_batch; hat_p_batch];
%     Cx0_batch = blkdiag(Cr_batch, Cp_batch)+0.01*eye(5);
%     pause(0.01)
    
%    d_batchekf(i:i+step-1)  = repmat( d_gaussian_wasserstein(gt,[hat_r_batch;hat_p_batch]),1,step);

pause(0.1)
if i+step < nrMeas
    delete(plotrmm)
%     delete(plotbatchekf)
end
alpha = 2 + exp(-T/tau)*(alpha - 2);
end
plotmeas=plot(y(1,:),y(2,:),'k.');



for j = 1:nrMeas
    

    [ x0,Cx0 ] = polynomial_monte_carlo(x0,Cx0,y(:,j),Cv,Ch);
    plotmc=plot_extent(x0,'-','c',2);
    
    
    CX0ekf2 = blkdiag(Cx0ekf2,Ch,Cv);
    X0ekf2 = [x0ekf2;0;0;0;0];
    [X0ekf2, CX0ekf2 ] = polynomial_alpha_shifted_ekf2(X0ekf2,CX0ekf2 ,y(:,j),f_func_g,f_jacobian,f_hessian);
    Cx0ekf2 = CX0ekf2(1:5,1:5)+ 0.01*eye(5);
    x0ekf2 = X0ekf2(1:5);
    plotekf2 = plot_extent(x0ekf2(1:5),'-','b',2);
    
    
    [hat_r_ekf, Cr_ekf, hat_p_ekf, Cp_ekf ] = updateEKF(eye(2),x0_ekf(1:2), Cx0_ekf(1:2,1:2),...
        x0_ekf(3:5), Cx0_ekf(3:5,3:5), y(:,j), Cv, Ch);
    x0_ekf = [hat_r_ekf; hat_p_ekf];
    Cx0_ekf = blkdiag(Cr_ekf, Cp_ekf)+0.01*eye(5);
    plotekf = plot_extent(x0_ekf,'-','m',2);
    
       
        d_mc(j) = d_gaussian_wasserstein(gt,x0);
        d_ekf2(j) = d_gaussian_wasserstein(gt,x0ekf2);
        d_ekf(j) = d_gaussian_wasserstein(gt,[hat_r_ekf; hat_p_ekf]);
        
        df_mc(j) = df_trace(gt,x0);
        df_ekf2(j) = df_trace(gt,x0ekf2);
        df_ekf(j) = df_trace(gt,[hat_r_ekf; hat_p_ekf]);
        

    
    pause(0.01)
    if j<nrMeas
        delete(plotekf)
        delete(plotekf2)
        delete(plotmc)
    end
end
legend([plotgt,plotmeas,plotrmm,plotekf,plotekf2,plotmc,plotbatchekf],{'Ground Truth','Measurement',....
    'Feldman','EKF','EKF2','Mento Carlo','batch'})


 figure;

hold on
plot(1:nrMeas,d_RMM,'r',1:nrMeas,d_ekf,'m',1:nrMeas,d_mc,'c',1:nrMeas,d_ekf2,'b',1:nrMeas,d_batchekf,'g')
legend({'Feldmann','EKF','Monte Carlo','EKF2','batch'})
title('Extension RMSE (Gaussian wasserstein)')
xlabel(['Cv: [' num2str(Cv(1,1)) ', ' num2str(Cv(2,2)), ']'])
box on
grid on

%  figure;
% 
% hold on
% plot(nrMeas,df_RMM,'r*',1:nrMeas,df_ekf,'m',1:nrMeas,df_mc,'c',1:nrMeas,df_ekf2,'b')
% legend({'Feldmann','EKF','Monte Carlo','EKF2'})
% title('Extension RMSE (difference between traces)')
% xlabel(['Cv: [' num2str(Cv(1,1)) ', ' num2str(Cv(2,2)), ']'])
% box on
% grid on

