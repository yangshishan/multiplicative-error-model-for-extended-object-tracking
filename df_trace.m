function d = df_trace(gt, est)
a_gt = gt(3);
a_est = est(3);

l1_gt = gt(4);
l2_gt = gt(5);

l1_est = est(4);
l2_est = est(5);

R_gt = [cos(a_gt), -sin(a_gt); sin(a_gt) cos(a_gt)];
R_est = [cos(a_est), -sin(a_est); sin(a_est) cos(a_est)];

X_gt = R_gt*diag([l1_gt^2 l2_gt^2])*R_gt';
X_est = R_est*diag([l1_est^2 l2_est^2])*R_est';

d=sqrt(trace((X_gt-X_est)^2));
end