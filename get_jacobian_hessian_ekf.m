function [f_jacobian,f_hessian ] = get_jacobian_hessian_ekf
syms  l1 l2 w h1 h2




 func_g =[(h1*l1*cos(w) - h2*l2*sin(w) );...
        ( h1*l1*sin(w) + h2*l2*cos(w))];
 X = [w;l1;l2;h1;h2];

jacobian_mat = jacobian(func_g,X);

for i = 1:numel(func_g)
    hessian_mat(:,:,i) = hessian(func_g(i),X);
    
end
jacobian_mat = subs(expand(jacobian_mat),[h1^2,h2^2],[1/4,1/4]);
hessian_mat = subs(expand(hessian_mat),[h1^2,h2^2],[1/4, 1/4]);



f_jacobian = matlabFunction(jacobian_mat, 'Vars', {[w, l1, l2,h1,h2]});
f_hessian = matlabFunction(hessian_mat, 'Vars', {[w, l1, l2,h1,h2]});
end