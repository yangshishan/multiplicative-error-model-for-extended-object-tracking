clc
clear
n=100000;
 mu_x=[1 1 pi/3 4 1];
 sigma_x = diag([2 2 .2 2 2]);
 x = mvnrnd(mu_x,sigma_x,n);
  for i = 1:n
      if mod(i,200)==1
          disp('t')
      end
      mu_y = x(i,1:2)';
      S=[cos(x(i,3)), -sin(x(i,3)); sin(x(i,3)) cos(x(i,3))]*diag([x(i,4),x(i,5)]);
      sigma_y = sigma_x(1:2,1:2)+S*diag([1/4 1/4])*S'+diag([3,4]);
      meas(i,:) = mvnrnd(mu_y,sigma_y,1);
  end
 figure
 hold on
 title('our case: measurements')
 subplot(1,3,1)
 plot(meas(:,1),meas(:,2),'.')
 axis equal

 subplot(1,3,2)
 histogram(meas(:,1),100)
 subplot(1,3,3)
 histogram(meas(:,2),100)
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
% % 1D
% mu_x = [1];
% sigma_x = diag([4]);
%  x = mvnrnd(mu_x,sigma_x,n);
%  for i = 1:n
%      mu_y = x(i).^2;
%      sigma_y = sigma_x;
%      y(i) = mvnrnd(mu_y,sigma_y,1);
%  end
%  figure
%  histogram(y,100)
%   title('1D histogram')
%  
%  % 2D
% mu_x = [1 1];
% alpha=pi/3;
% sigma_x = [cos(alpha) -sin(alpha); sin(alpha) cos(alpha)]*diag([4 1])*[cos(alpha) -sin(alpha); sin(alpha) cos(alpha)]';
%  x = mvnrnd(mu_x,sigma_x,n);
%  for i = 1:n
%      mu_y = x(i,:).^2;
%      sigma_y = sigma_x;
%      y2D(i,:) = mvnrnd(mu_y,sigma_y,1);
%  end
%  figure
%  hold on
%  title('2D points')
%  plot(y2D(:,1),y2D(:,2),'.')
%  
%  
%  



% our case 

  