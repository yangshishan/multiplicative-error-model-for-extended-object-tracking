clc
clear

nr_samples = 100000;

Ch = diag([.25 .25]);
Ca = 0.1;
Cl = diag([2 2]);
Cp = blkdiag(Ca,Cl);

bar_a = sqrt(3)/2;
bar_l = [1 3]';


a_samples = mvnrnd(bar_a, Ca,nr_samples)';
l_samples = mvnrnd(bar_l, Cl,nr_samples)';
h_samples = mvnrnd([0 0], Ch,nr_samples)';

S = @(a,l1,l2) [cos(a) -sin(a); sin(a) cos(a)]*diag([l1,l2]);
J1 = @(a,l1,l2) [-l1*sin(a) cos(a) 0; -l2*cos(a) 0 -sin(a)];
J2 = @(a,l1,l2) [l1*cos(a) sin(a) 0; -l2*sin(a) 0 cos(a)];
for i = 1:nr_samples
a = a_samples(i);
l1 = l_samples(1,i);
l2 = l_samples(2,i);
h1 = h_samples(1,i);
h2 = h_samples(2,i);
h = [h1;h2];

hat_J1 = J1(a,l1,l2);
hat_J2 = J2(a,l1,l2);
hat_S = S(a,l1,l2);

part1(:,i) = [h'*hat_J1;h'*hat_J2]*([a;l1;l2]-[bar_a;bar_l]);
part2(:,i) = hat_S*h;
parts(:,i) = [part1(:,i); part2(:,i)];
end
Csh = cov(part2');
Chj = cov(part1');
bar_S = S(bar_a, bar_l(1),bar_l(2)); 
bar_J1 = J1(bar_a, bar_l(1),bar_l(2));
bar_J2 = J2(bar_a, bar_l(1),bar_l(2));

sgm11 = trace(bar_J1'*Ch*bar_J1*Cp);
sgm22 = trace(bar_J2'*Ch*bar_J2*Cp);
sgm12 = trace(bar_J1'*Ch*bar_J2*Cp);
bar_S*Ch*bar_S' + [sgm11 sgm12; sgm12 sgm22] -Csh - Chj

clc
clear

syms a l1 l2 c11 c12 c13 c22 c23 c33
J1 =  [-l1*sin(a) cos(a) 0; -l2*cos(a) 0 -sin(a)];
J2 =  [l1*cos(a) sin(a) 0; -l2*sin(a) 0 cos(a)];
Ch = diag([.25,.25]);
Cp = [c11 c12 c13; c12 c22 c23; c13 c23 c33]

% sgm11 = trace(J1.'*Ch*J1*Cp);
% sgm22 = trace(J2.'*Ch*J2*Cp);
% sgm12 = trace(J1.'*Ch*J2*Cp);
% 
% expand([sgm11 sgam12; sgm12 sgm22])


