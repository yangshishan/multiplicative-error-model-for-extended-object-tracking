


% close all
clc
clear
dbstop error
set(0,'defaulttextinterpreter','latex')

%% parameters

mc_runs = 1000;
% nr of measurements we get follows a possion distribution
possion_lambda = 15;
H = [1 0 0 0; 0 1 0 0]; % matrix maps kinematic state into position
H_velo = [zeros(2,2),eye(2)];


%% generate ground truth
[gt_kin,gt_par, time_steps, delta_t] =get_ground_truth;

d_RMM=zeros(mc_runs,time_steps);
d_RMMLan=zeros(mc_runs,time_steps);
d_ekf=zeros(mc_runs,time_steps);

v_RMM=zeros(mc_runs,time_steps);
v_RMMLan=zeros(mc_runs,time_steps);
v_batchEKF=zeros(mc_runs,time_steps);
%% setting prior
hat_r0 = [100,100,5,-8]'; % kinematic state: position and velocity
hat_p0 = [-pi/5,200,90]'; % shape variable: orientation and semi-axes lengths
% hat_r0 = gt_kin(:,1);
% hat_p0=gt_par(3:5,1);
hat_x0 = [hat_r0; hat_p0];

C_r0 = blkdiag(1600*eye(2),16*eye(2));
C_p0 = blkdiag(.5,60^2*eye(2));
C_x0 = blkdiag(C_r0,C_p0);


% parameters for EKF
C_h = diag([1/4, 1/4]);
C_v = diag([100^2,20^2]);

Ar = [eye(2),delta_t*eye(2); zeros(2,2),eye(2)];
Ap = eye(3);

C_w_r = blkdiag(100*eye(2),eye(2)); % process noise covariance for kinematic state
C_w_p = blkdiag(.02,4*eye(2)); % process noise covariance for shape variable

for it = 1:mc_runs
    
    
    hat_r_ekf = hat_r0; % kinematic state: position and velocity
    hat_p_ekf = hat_p0;
    Cr_ekf = C_r0;
    Cp_ekf = C_p0;
    
    
    % parameters for Random Matrix
    alpha = 50;
    tau = 30;
    T = 10;
    const_z = 1/4;
    hat_x_RMM = hat_r0;
    hat_X_RMM = get_random_matrix_state(hat_p0);
    Cx_RMM = C_r0;
    
    alphaLan = 50;
    hat_x_RMMLan = hat_r0;
    hat_X_RMMLan = get_random_matrix_state(hat_p0);
    Cx_RMMLan = C_r0;
    delta = 40;
    A = eye(2)/(delta)^(.5);
%     figure
    
    for t = 1:time_steps
        N = poissrnd(possion_lambda);
        while N == 0||N==1
            N = poissrnd(possion_lambda);
        end
        disp(['iteration: ' num2str(it) ', time: ' num2str(t) ]);
        
        %% ------------------get measurements------------------------------------
        gt_cur_par = gt_par(:,t);
        gt_velo = H_velo*gt_kin(:,t);
        gt_rot = [cos(gt_cur_par(3)), -sin(gt_cur_par(3)); sin(gt_cur_par(3)), cos(gt_cur_par(3))];
        gt_len = gt_cur_par(4:5);
        y = zeros(2,N);
        for n = 1:N
            h_noise(n,:) = -1 + 2.*rand(1,2);
            while norm(h_noise(n,:)) > 1
                h_noise(n,:) = -1 + 2.*rand(1,2);
            end
            y(:,n) = H*gt_kin(:,t) + gt_rot*diag(gt_len)*h_noise(n,:)'+ mvnrnd([0 0], C_v, 1)';
        end
        %% update RMM
        meas_mean = mean(y,2);
        meas_spread = (N - 1) * cov(y');
        
        [hat_x_RMM, hat_X_RMM, C_x_RMM, alpha_update]...
            = updateRMM(hat_x_RMM, hat_X_RMM, Cx_RMM, alpha,meas_mean, ...
            meas_spread, C_v,N,H,const_z);
        
        [~, len_RMM,ang_RMM] = get_random_matrix_ellipse(hat_X_RMM);
        rmm_par = [H*hat_x_RMM; ang_RMM;len_RMM];
        
        
        [hat_x_RMMLan, hat_X_RMMLan, C_x_RMMLan, alpha_updateLan] = updateRMMLan(hat_x_RMMLan, hat_X_RMMLan, ...
            Cx_RMMLan, alphaLan,meas_mean, meas_spread, C_v,N,H);
        
        [~, len_RMMLan,ang_RMMLan] = get_random_matrix_ellipse(hat_X_RMMLan);
        
        rmmLan_par = [H*hat_x_RMMLan; ang_RMMLan;len_RMMLan];
        
        
        
        %         [ hat_r_batchEKF, Cr_batchEKF,hat_p_batchEKF, Cp_batchEKF ] = updateBatchEKF...
        %             (H,hat_r_batchEKF, Cr_batchEKF, hat_p_batchEKF, Cp_batchEKF, y, C_v, C_h);
        for i = 1:size(y,2)
            [hat_r_ekf, Cr_ekf, hat_p_ekf, Cp_ekf ] = updateEKF(H,hat_r_ekf,Cr_ekf,...
                hat_p_ekf, Cp_ekf, y(:,i), C_v, C_h);
        end
        
        
%          if mod(t,3)==1
%         meas_points=plot( y(1,:)/1000, y(2,:)/1000, '.k','lineWidth',0.5);
%         
%         hold on
%         
%         
%         
%         gt_plot = plot_extent([gt_cur_par(1:2)/1000; gt_cur_par(3);gt_cur_par(4:5)/1000], '-','k',1);
%         axis equal
%         
%         est_plot_rmm = plot_extent([rmm_par(1:2)/1000;rmm_par(3);rmm_par(4:5)/1000],'-','g',1);
%         est_plot_rmmLan = plot_extent([rmmLan_par(1:2)/1000;rmmLan_par(3);rmmLan_par(4:5)/1000],'-','b',1);
%         
%         est_plot_ekf = plot_extent([H*hat_r_ekf/1000;hat_p_ekf(1);hat_p_ekf(2:3)/1000],'-', 'm', 1);
%         %    text(rmm_par(1)/1000,rmm_par(2)/1000,num2str(t))
%     end
        
        
        %% evaluation
        
        d_RMM(it,t) = d_gaussian_wasserstein(gt_cur_par,rmm_par);
        d_RMMLan(it,t) = d_gaussian_wasserstein(gt_cur_par,rmmLan_par);
        %         d_batchEKF(it,t) = d_gaussian_wasserstein(gt_cur_par,[H*hat_r_batchEKF; hat_p_batchEKF]);
        d_ekf(it,t) = d_gaussian_wasserstein(gt_cur_par,[H*hat_r_ekf; hat_p_ekf]);
        
        
        df_RMM(it,t) = df_trace(gt_cur_par,rmm_par);
        df_RMMLan(it,t) = df_trace(gt_cur_par,rmmLan_par);
        %         df_batchEKF(it,t) = df_trace(gt_cur_par,[H*hat_r_batchEKF; hat_p_batchEKF]);
        df_ekf(it,t) = df_trace(gt_cur_par,[H*hat_r_ekf; hat_p_ekf]);
        
        
        
        v_RMM(it,t) = norm(gt_velo - H_velo*hat_x_RMM);
        v_RMMLan(it,t) = norm(gt_velo - H_velo*hat_x_RMMLan);
        %         v_batchEKF(it,t) = norm(gt_velo - H_velo*hat_r_batchEKF);
        v_ekf(it,t) = norm(gt_velo - H_velo*hat_r_ekf);
        %%  predict　RMM
        [hat_x_RMM, hat_X_RMM,Cx_RMM, alpha] = predictRMM(....
            hat_x_RMM, hat_X_RMM, C_x_RMM, alpha_update,Ar,C_w_r,T,tau);
        [hat_x_RMMLan, hat_X_RMMLan,Cx_RMMLan, alphaLan] = predictRMMLan(....
            hat_x_RMMLan, hat_X_RMMLan, C_x_RMMLan, alpha_updateLan,Ar,C_w_r,A,delta);
        %         [hat_r_batchEKF,Cr_batchEKF, hat_p_batchEKF,Cp_batchEKF] = predictEKF(Ar,Ap, ...
        %             hat_r_batchEKF, hat_p_batchEKF, Cr_batchEKF, Cp_batchEKF,C_w_r, C_w_p);
        [hat_r_ekf,Cr_ekf, hat_p_ekf,Cp_ekf] = predictEKF(Ar,Ap, ...
            hat_r_ekf, hat_p_ekf, Cr_ekf, Cp_ekf,C_w_r, C_w_p);
    end
end

figure;
hold on
plot(1:time_steps,mean(d_RMM,1),'g',1:time_steps,mean(d_ekf,1),'r',1:time_steps,mean(d_RMMLan,1),'b')
legend({'Feldmann','batchEKF','Lan'})
title('Extension RMSE (Gaussian wasserstein)')
box on
grid on

figure;
hold on
plot(1:time_steps,sqrt(mean(df_RMM,1)),'g',1:time_steps,sqrt(mean(df_ekf,1))...
    ,'r',1:time_steps,sqrt(mean(df_RMMLan,1)),'b')
legend({'Feldmann','batchEKF','Lan'})
title('Extension RMSE trace diff')
box on
grid on

figure;
hold on
plot(1:time_steps,mean(v_RMM,1),'g',1:time_steps,mean(v_ekf,1),'r',1:time_steps,mean(v_RMMLan,1),'b')
legend({'Feldmann','batchEKF','Lan'})
title('Velocity RMSE')
box on
grid on
