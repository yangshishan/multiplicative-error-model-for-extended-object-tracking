function [x_pred, X_pred,P_pred, alpha_pred] = predictRMMLanExact(....
    x, X, P, alpha,F,Q,A,delta)

d=2;

x_pred = kron(F,eye(2))*x;
P_pred = F*P*F'+Q;

lambda = alpha - 2*d-2;
alpha_pred = ((2*delta*(lambda+1)*(lambda-1)*(lambda-2))/(lambda^2*(lambda+delta)))+2*d+4;
X_pred = (delta/lambda)*(alpha_pred-2*d-2)*A*X*A';





end