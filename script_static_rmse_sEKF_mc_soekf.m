% close all
clc
clear all
% rng default
% load('s.mat')
% rng(s)
gt = [0 0 sqrt(3)/2 2 9]';


Cv = 1*diag([2 9]);
Ch = diag([1/4,1/4]);

nrMeas = 100;

rotMatrix = @(alpha) [cos(alpha) -sin(alpha); sin(alpha) cos(alpha)];

[f_func_g,f_jacobian,f_hessian] = get_jacobian_hessian_ekf2('alpha','static',1/4,1/4);

gtRot = rotMatrix(gt(3));
for it = 1:200
    x0 = [1 1 0 1 12]';
    Cx0 = diag([1 1 1 2 9]);
    
    x0_ekf = x0;
    Cx0_ekf = Cx0;


    x0ekf2 = x0;
    Cx0ekf2 = Cx0;
    
    alpha = 5;
    tau = 200;
    T = 10;
    const_z = 1/4;
    hat_x_RMM = x0(1:2);
    hat_X_RMM = get_random_matrix_state(x0(3:5));
    Cx_RMM = Cx0(1:2,1:2);
    
    for n = 1:nrMeas
        h(n,:) = -1 + 2.*rand(1,2);
        while norm(h(n,:)) > 1
            h(n,:) = -1 + 2.*rand(1,2);
        end
        y(:,n) = gt(1:2) + gtRot*diag(gt(4:5))*h(n,:)'+ mvnrnd([0 0], Cv, 1)';
    end
    
    
    
    step = 10;
for i = 1:step:nrMeas
[hat_x_RMM, hat_X_RMM, C_x_RMM, alpha_update]= updateRMM(hat_x_RMM, hat_X_RMM, Cx_RMM, alpha,mean(y,2), ...
    (nrMeas - 1) * cov(y(:,i:i+step-1)'), Cv,nrMeas,eye(2),const_z);

[~, len_RMM,ang_RMM] = get_random_matrix_ellipse(hat_X_RMM);
rmm_par = [hat_x_RMM; ang_RMM;len_RMM];
 d_RMM(it,i:i+step-1) = repmat( d_gaussian_wasserstein(gt,rmm_par),1,step);
alpha = 2 + exp(-T/tau)*(alpha - 2);
end
    

    
    for j = 1:nrMeas
        disp(['iteration: ' num2str(it) ', measurement: ' num2str(j)])
        
        [ x0,Cx0 ] = polynomial_monte_carlo(x0,Cx0,y(:,j),Cv,Ch);
        Cx0=Cx0+0.001*eye(5);
        
        
        CX0ekf2 = blkdiag(Cx0ekf2,Ch,Cv);
        X0ekf2 = [x0ekf2;0;0;0;0];
        [X0ekf2, CX0ekf2 ] = polynomial_alpha_shifted_ekf2(X0ekf2,CX0ekf2 ,y(:,j),f_func_g,f_jacobian,f_hessian);
        Cx0ekf2 = CX0ekf2(1:5,1:5)+ 0.001*eye(5);
        x0ekf2 = X0ekf2(1:5);
        
        
        
        [hat_r_ekf, Cr_ekf, hat_p_ekf, Cp_ekf ] = updateEKF(eye(2),x0_ekf(1:2), Cx0_ekf(1:2,1:2),...
            x0_ekf(3:5), Cx0_ekf(3:5,3:5), y(:,j), Cv, Ch);
        x0_ekf = [hat_r_ekf; hat_p_ekf];
        Cx0_ekf = blkdiag(Cr_ekf, Cp_ekf)+0.001*eye(5);
        

        d_mc(it,j) = d_gaussian_wasserstein(gt,x0);
        d_ekf2(it,j) = d_gaussian_wasserstein(gt,x0ekf2);
        d_ekf(it,j) = d_gaussian_wasserstein(gt,[hat_r_ekf; hat_p_ekf]);
        
        
    end
           
    
end

figure;
hold on
plot(1:nrMeas,mean(d_RMM,1),'r',1:nrMeas,mean(d_ekf,1),'m',1:nrMeas,mean(d_mc,1),'c',1:nrMeas,mean(d_ekf2,1),'b')
legend({'Feldmann','EKF','Monte Carlo','EKF2'})
title('Extension RMSE (Gaussian wasserstein)')
xlabel(['Cv: [' num2str(Cv(1,1)) ', ' num2str(Cv(2,2)), ']'])
box on
grid on
