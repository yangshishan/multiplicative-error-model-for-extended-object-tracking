function [ r_hat, C_r,p_hat, C_p ] = updateBatchEKFmc(H,r_hat, C_ro, p_hat, C_p, y, C_v, C_h)

n=size(y,2);
[E_Y, C_YY,C_pY] = mc_pseudo(r_hat, C_ro, p_hat, C_p, C_v, C_h,n);
[S_hat,ds,M_hat] = getDsM(p_hat,C_h);
%
mean_y = mean(y,2);
E_y = H*r_hat;
C_ry = C_ro*H';

for i = 1:2
    for j = 1:2
        int_c(i,j) = trace(ds(:,:,i)'*C_h*ds(:,:,j)*C_p);
        
    end
end
%
C_yy = H*C_ro*H' +(S_hat*C_h*S_hat')/n +int_c/n+C_v/n ;

% kinematic state update
r_hat = r_hat + C_ry*(C_yy)^(-1)*(mean_y-E_y);
C_r = C_ro - C_ry*(C_yy)^(-1)*C_ry';
C_r = (C_r+C_r')/2;

%% pseudo measurement
sum_Y = 0;
for i = 1:n
    % sum_Y = sum_Y + (y(:,i)-mean_y)*(y(:,i)-mean_y)';
    sum_Y = sum_Y + (y(:,i))*(y(:,i))';
end
cov_y = sum_Y/n;

Y = [cov_y(1,1);cov_y(2,2);cov_y(1,2)];

% C_yy = H*C_ro*H' +S_hat*C_h*S_hat'+ int_c +C_v ;
% sgm11 =  C_yy(1,1);
% sgm12 = C_yy(1,2);
% sgm22 = C_yy(2,2);
% E_Y =  [sgm11; sgm22;sgm12];
% C_YY = [2*sgm11^2, 2*sgm12^2 , 2*sgm11*sgm12;
%     2*sgm12^2, 2*sgm22^2,2*sgm22*sgm12;
%     2*sgm11*sgm12, 2*sgm22*sgm12,sgm11*sgm22+sgm12^2]/n;

% C_pY =  (C_p)*(M_hat/n)';

p_hat = p_hat + C_pY*(C_YY)^(-1)*(Y-E_Y);


C_p = C_p - C_pY*(C_YY)^(-1)*C_pY';

C_p = (C_p + C_p')/2;
end
function [E_Y, C_YY,C_pY] = mc_pseudo(r_hat, C_ro, p_hat, C_p, C_v, C_h,n)
nr_samples = 100000;
kin_dim = numel(r_hat);
rsamples = mvnrnd(r_hat, C_ro,nr_samples)';

psamples = mvnrnd(p_hat, C_p,nr_samples)';



for s = 1:nr_samples
    vsamples = mvnrnd([0 0], C_v, n)';
    hsamples = mvnrnd([0 0], C_h,n)';
    for i = 1:n
        
        c = rsamples(1:2,s);
        p = psamples(:,s);
        h = hsamples(:,i);
        v = vsamples(:,i);
        
        
        h1=h(1);
        h2=h(2);
        v1=v(1);
        v2=v(2);
        c1=c(1);
        c2=c(2);
        w = p(1);
        l1=p(2);
        l2=p(3);
        ysamples (:,i) = [(c1 + h1*l1*cos(w) - h2*l2*sin(w) + v1);...
            (c2 + h1*l1*sin(w) + h2*l2*cos(w) + v2)];
%         rysamples (:,i) =[rsamples(:,i);ysamples(:,i)];
        quad_samples(:,i)=[ysamples(1,i)^2; ysamples(2,i)^2;ysamples(1,i)*ysamples(2,i)];
    end
    Ysamples(:,s) = [sum(quad_samples(1,:));sum(quad_samples(2,:));sum(quad_samples(3,:))]/n;
    pYsamples(:,s)=[psamples(:,s);Ysamples(:,s)];
end

% E_y = mean(ysamples,2);
% C_y = cov(ysamples');

E_Y=mean(Ysamples,2);
C_YY = cov(Ysamples');
cross_pY = cov(pYsamples');
% cross_ry = cov(rysamples');
C_pY = cross_pY(1:3,4:6);
% C_ry = cross_ry(1:kin_dim,kin_dim+1:end);
end
function [S, ds, M] = getDsM(p,C_h)
a = p(1);
l1 = p(2);
l2 = p(3);
S = [cos(a) -sin(a); sin(a) cos(a)]*diag([l1 l2]);
ds(:,:,1) = [-l1*sin(a) cos(a) 0; -l2*cos(a) 0 -sin(a)];
ds(:,:,2) = [l1*cos(a) sin(a) 0; -l2*sin(a) 0 cos(a)];

M = [2*S(1,:)*C_h*ds(:,:,1);2*S(2,:)*C_h*ds(:,:,2);S(1,:)*C_h*ds(:,:,2)+S(2,:)*C_h*ds(:,:,1)];

end