function lambda2 = get_cov_likelihood(sum_y,p,C_v,n)
Xj = get_random_matrix_state(p);
Yj = Xj;

deltaj = sum_y-(n-1)*Yj;
var_delta_Yj = (n-1)*(trace(Yj)+Yj^2);
Vj = trace(Xj)*Xj+Xj^2;
var_deltaj = var_delta_Yj + 0.25^2*(n-1)^2*Vj;


lambda2 = det(2*pi*var_deltaj)^(-3/4)*exp(trace(-.5*deltaj*(var_deltaj)^-1*deltaj));
end