clear
syms m1 m2 e1 e2 l1 l2 a1 a2 b1 b2 h1 h2 v1 v2  w
syms c11 c12 c22 c33 c34 c35 c44 c45 c55 ch1 ch2 cv1 cv2
Cr = [c11 c12; c12 c22];
Cp = [c33 c34 c35;c34 c44 c45; c35 c45 c55];
Ch = diag([ch1 ch2]);
Cv = diag([cv1 cv2]);
X_cov = blkdiag(Cp);

    func_g =[(h1*l1*cos(w) - h2*l2*sin(w) );...
        ( h1*l1*sin(w) + h2*l2*cos(w))]
    s =[l1*cos(w), -l2*sin(w );...
        l1*sin(w),  l2*cos(w)]
 X = [w;l1;l2];

jacobian_mat = jacobian(func_g,X);


for i = 1:numel(func_g)
    hessian_mat(:,:,i) = hessian(func_g(i),X);
    
end
for i = 1:2
    for j = 1:2
        tic
        C_yy(i,j) = jacobian_mat(i,:)*X_cov*jacobian_mat(j,:).'+...
            (1/2)*trace(hessian_mat(:,:,i)*X_cov*hessian_mat(:,:,j)*X_cov);
        
    end
end
expand(C_yy(1,1))-expand(jacobian_mat(1,:)*X_cov*jacobian_mat(1,:).')-...
    expand(1/2*trace(hessian_mat(:,:,1)*X_cov*hessian_mat(:,:,1)*X_cov))
expand(C_yy(1,2))-expand(jacobian_mat(1,:)*X_cov*jacobian_mat(2,:).')-...
    expand(1/2*trace(hessian_mat(:,:,1)*X_cov*hessian_mat(:,:,2)*X_cov))
expand(C_yy(2,2))-expand(jacobian_mat(2,:)*X_cov*jacobian_mat(2,:).')-...
    expand(1/2*trace(hessian_mat(:,:,2)*X_cov*hessian_mat(:,:,2)*X_cov))

expand(jacobian_mat(1,:)*C*jacobian_mat(1,:).')
expand(jacobian_mat(1,:)*C*jacobian_mat(2,:).')
expand(jacobian_mat(2,:)*C*jacobian_mat(2,:).')
expand(1/2*trace(hessian_mat(:,:,1)*C*hessian_mat(:,:,1)*C))
expand(1/2*trace(hessian_mat(:,:,1)*C*hessian_mat(:,:,2)*C))
expand(1/2*trace(hessian_mat(:,:,2)*C*hessian_mat(:,:,2)*C))

S =Cr+ [cos(w) -sin(w); sin(w) cos(w)]*diag([l1 l2])*diag([ch1, ch2])*diag([l1 l2])*[cos(w) -sin(w); sin(w) cos(w)].' + Cv

