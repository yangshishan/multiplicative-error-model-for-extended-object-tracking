% close all
clc
clear
dbstop error
set(0,'defaulttextinterpreter','latex')

%% parameters
motionmodel = {'NCV'};
% nr of measurements we get follows a possion distribution
possion_lambda = 20;
H = [1 0 0 0; 0 1 0 0]; % matrix maps kinematic state into position
H_velo = [zeros(2,2),eye(2)];
% parameters for EKF
C_h = diag([1/4, 1/4]);
C_v = diag([50^2,50^2]);


%% generate ground truth
[gt_kin,gt_par, time_steps, delta_t] =get_ground_truth;

%% setting prior
hat_r0 = [100,100,5,-8]'; % kinematic state: position and velocity
hat_p0 = [-pi/3,200,90]'; % shape variable: orientation and semi-axes lengths
% hat_r0 = gt_kin(:,1);
% hat_p0=gt_par(3:5,1);
hat_x0 = [hat_r0; hat_p0];

C_r0 = blkdiag( 900*eye(2),16*eye(2));
C_p0 = blkdiag(1*eye(1),3600*eye(2));

X{1} = get_random_matrix_state(hat_p0);
X{2}=X{1};
X{3}=X{1};

Ar = [eye(2),delta_t*eye(2); zeros(2,2),eye(2)];
Ap = eye(3);

C_w_r = blkdiag(100*eye(2),16*eye(2)); % process noise covariance for kinematic state




hat_r_batchEKF = hat_r0; % kinematic state: position and velocity
hat_p_batchEKF = hat_p0;
P{1} = C_r0;
P{2} = C_r0;
P{3} = C_r0;
x{1} = hat_r0;
x{2} = hat_r0;
x{3} = hat_r0;



MU_ip = [0.25 0.5 0.25];
pij = [0.6 0.2 0.2;
       0.2 0.7 0.1;
       0.2 0.1 0.7];
        
 A{1}=Ar;
 A{2}=Ar;
 A{3}=Ar;
 Q{1}=blkdiag(eye(2),eye(2));
 Q{2}=blkdiag(100*eye(2),16*eye(2));
 Q{3}=blkdiag(1000*eye(2),90*eye(2));
R{1}= C_v;
R{2}=C_v;
R{3}=C_v;

 ind{1}=[1 2];
 ind{2}=[1 2];
 ind{3}=[1 2];
 
% parameters for SOEKF
sgh1 = 1/4;
sgh2 = 1/4;








% parameters for Random Matrix
alpha{1} = 50;
alpha{2} = 50;
alpha{3} = 50;
tau{1} = 2;
tau{2}=20;
tau{3}=100;
delta = 20;

T = 10;
const_z = 1/4;
dims = 2;

figure;

hold on

for t = 2:time_steps
    N = poissrnd(possion_lambda);
    while N == 0
        N = poissrnd(possion_lambda);
    end
    disp(['time step:' num2str(t) ', ' num2str(N) ' measurements']);
    
    %% ------------------get measurements------------------------------------
    gt_cur_par = gt_par(:,t);
    gt_velo = H_velo*gt_kin(:,t);
    gt_rot = [cos(gt_cur_par(3)), -sin(gt_cur_par(3)); sin(gt_cur_par(3)), cos(gt_cur_par(3))];
    gt_len = gt_cur_par(4:5);
    y = zeros(2,N);
    for n = 1:N
        h_noise(n,:) = -1 + 2.*rand(1,2);
        while norm(h_noise(n,:)) > 1
            h_noise(n,:) = -1 + 2.*rand(1,2);
        end
        y(:,n) = H*gt_kin(:,t) + gt_rot*diag(gt_len)*h_noise(n,:)'+ mvnrnd([0 0], C_v, 1)';
    end
    
    
[x,P,mu_ip,X,alpha,x_fuse,X_fuse,P_fuse,alpha_fuse] = imm_RMMFeldmann_filter...
    (x,P,X,alpha,MU_ip,pij,ind,dims,H,A,Q,y,R,T,tau);

MU(:,t)=mu_ip';
    
    if mod(t,3)==2
        meas_points=plot( y(1,:)/1000, y(2,:)/1000, '.k','lineWidth',0.5);
        
        hold on
        
    [~, len_RMM,ang_RMM] = get_random_matrix_ellipse(X_fuse);
        rmm_par = [H*x_fuse; ang_RMM;len_RMM];

        gt_plot = plot_extent([gt_cur_par(1:2)/1000; gt_cur_par(3);gt_cur_par(4:5)/1000], '-','k',1);
        axis equal
        
         est_plot_rmm = plot_extent([rmm_par(1:2)/1000;rmm_par(3);rmm_par(4:5)/1000],'-','g',1);
%         est_plot_rmmLan = plot_extent([rmmLan_par(1:2)/1000;rmmLan_par(3);rmmLan_par(4:5)/1000],'-','b',1);
%         est_plot_batchekf = plot_extent([x(1:2)/1000;x(5);x(6:7)/1000],'-', 'm', 1);
        %    text(rmm_par(1)/1000,rmm_par(2)/1000,num2str(t))
    end
    
end

figure
plot(1:time_steps,MU(1,:),'g-',1:time_steps,MU(2,:),'b-',1:time_steps,MU(3,:),'r-')