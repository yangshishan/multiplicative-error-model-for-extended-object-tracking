function [x_allM,P_allM,MU,X_allM,alpha_allM,x_fuse,X_fuse,P_fuse,alpha_fuse] = imm_RMMFeldmann_filter...
    (xp_allM,Pp_allM,Xp_allM,alpha,MU_ip,p_ij,ind,dims,H,A,Q,y,R,T,tau)


% Number of models
m = length(xp_allM);
kin_dims = numel(xp_allM{1});

% Normalizing factors for mixing probabilities
c_j = zeros(1,m);
for j = 1:m
    for i = 1:m
        c_j(j) = c_j(j) + p_ij(i,j).*MU_ip(i);
    end
end

% Mixing probabilities
MU_ij = zeros(m,m);
for i = 1:m
    for j = 1:m
        MU_ij(i,j) = p_ij(i,j) * MU_ip(i) / c_j(j);
    end
end

for j = 1:m
    Vj =(alpha{j}*trace(Xp_allM{j})*Xp_allM{j}+(alpha{j}+2)*Xp_allM{j}^2)/((alpha{j}+1)*(alpha{j}-2));
    e_k{j}=trace(Vj);
end
% Calculate the mixed state mean for each filter
x_0j = cell(1,m);
P_0j = cell(1,m);

X_0j = cell(1,m);
e_0j = cell(1,m);
alpha_0j = cell(1,m);
for j = 1:m
     x_0j{j} = zeros(kin_dims,1);
    P_0j{j} = zeros(kin_dims,kin_dims);
    X_0j{j} = zeros(dims,dims);
 e_0j{j}=0;
    for i = 1:m
        x_0j{j} = x_0j{j} + xp_allM{i}*MU_ij(i,j);
        P_0j{j} = P_0j{j} + MU_ij(i,j)*(Pp_allM{i} + (xp_allM{i}-x_0j{j})*(xp_allM{i}-x_0j{j})');
        X_0j{j} = X_0j{j} + Xp_allM{i}*MU_ij(i,j);
        

        e_0j{j}= e_0j{j} + (e_k{i}+trace((Xp_allM{i}-X_0j{j})^2))*MU_ij(i,j);
        pk = e_0j{j}+trace(Xp_allM{i})^2+trace(Xp_allM{i}^2);
        qk = 2*(e_0j{j}+trace(Xp_allM{i}^2));
        alpha_0j{j}=(pk+sqrt(pk^2+4*e_0j{j}*qk))/2*e_0j{j};
    end
end




% Space for estimates
x_p = cell(1,m);
P_p = cell(1,m);
X_p = cell(1,m);
alpha_p = cell(1,m);
x_allM = cell(1,m);
P_allM = cell(1,m);
X_allM = cell(1,m);
alpha_allM = cell(1,m);
lambda = zeros(1,m);
n_k = size(y,2);
   bary = mean(y,2);
    barY = (n_k - 1) * cov(y');

% Filter the estimates for each model
for i = 1:m
    % Predict the estimates
    [x_p{i}, X_p{i},P_p{i},alpha_p{i}] = predictRMM(...
        x_0j{i},X_0j{j},P_0j{i},alpha_0j{j},A{i},Q{i},T,tau{j});
    
   
    % Update the estimates
    [x_allM{i},X_allM{i}, P_allM{i},alpha_allM{i}] = updateRMM(...
        x_p{i},X_p{i},P_p{i},alpha_p{i},bary,barY,R{i},n_k,H,.25);
    

    
    % Calculate likelihoods
  lambda(i) = measRMMFeldmann_lhood(x_allM{i},X_allM{i},P_allM{i},alpha_allM{i},bary,barY,n_k,H,R{i});

end

% Calculate the model probabilities

c = sum(lambda.*c_j);
MU = c_j.*lambda/c;
for j = 1:m
    Vj_update =(alpha_allM{j}*trace(X_allM{j})*X_allM{j}+(alpha_allM{j}+2)*X_allM{j}^2)/((alpha_allM{j}+1)*(alpha_allM{j}-2));
    e_k_update{j}=trace(Vj_update);
end

% Output the combined updated state mean and covariance, if wanted.
if nargout > 3
    % Space for estimates
    x_fuse = zeros(kin_dims,1);
    P_fuse = zeros(kin_dims,kin_dims);
    X_fuse = zeros(dims,dims);
    e_fuse  = 0;
    alpha_fuse =0;
    % Updated state mean
    for i = 1:m
        x_fuse = x_fuse + MU(i)*x_allM{i};
        P_fuse = P_fuse + MU(i)*(P_allM{i} + (x_allM{i}-x_0j{i})*(x_allM{i}-x_0j{i})');
        X_fuse = X_fuse+ MU(i)*X_allM{i};
        
        e_fuse = e_fuse + (e_k_update{i}+trace((X_allM{i}-X_0j{i})^2))*MU(i);
        pk = e_k_update{j}+trace(X_allM{i})^2+trace(X_allM{i}^2);
        qk = 2*(e_k_update{j}+trace(X_allM{i}^2));
        alpha_fuse=(pk+sqrt(pk^2+4*e_fuse*qk))/2*e_fuse;
    end

end


end