% close all
clc
clear
dbstop warning
set(0,'defaulttextinterpreter','latex')

%% parameters
motionmodel = {'NCV'};
% nr of measurements we get follows a possion distribution
possion_lambda = 20;
H = [1 0 0 0; 0 1 0 0]; % matrix maps kinematic state into position
H_velo = [zeros(2,2),eye(2)];
% parameters for EKF
C_h = diag([1/4, 1/4]);
C_v = diag([50^2,50^2]);


%% generate ground truth
[gt_kin,gt_par, time_steps, delta_t] =get_ground_truth;

%% setting prior
hat_r0 = [100,100,5,-8]'; % kinematic state: position and velocity
hat_p0 = [-pi/3,200,90]'; % shape variable: orientation and semi-axes lengths
% hat_r0 = gt_kin(:,1);
% hat_p0=gt_par(3:5,1);
hat_x0 = [hat_r0; hat_p0];

C_r0 = blkdiag( 900*eye(2),16*eye(2));
C_p0 = blkdiag(1*eye(1),3600*eye(2));
C_x0 = blkdiag(C_r0,C_p0);



Ar = [eye(2),delta_t*eye(2); zeros(2,2),eye(2)];
Ap = eye(3);

C_w_r = blkdiag(100*eye(2),16*eye(2)); % process noise covariance for kinematic state
C_w_p = blkdiag(0.001,eye(2)); % process noise covariance for shape variable

hat_r_EKF = hat_r0; % kinematic state: position and velocity
hat_p_EKF = hat_p0;
Cr_EKF = C_r0;
Cp_EKF = C_p0;


hat_r_batchEKF = hat_r0; % kinematic state: position and velocity
hat_p_batchEKF = hat_p0;
Cr_batchEKF = C_r0;
Cp_batchEKF = C_p0;
xEKF{1} = [hat_r0; hat_p0];
xEKF{2} = [hat_r0; hat_p0];
xEKF{3} = [hat_r0; hat_p0];

Cx_EKF{1}=blkdiag(C_r0,C_p0);
Cx_EKF{2}=blkdiag(C_r0,C_p0);
Cx_EKF{3}=blkdiag(C_r0,C_p0);

MU_EKF = [0.9 0.05 0.05];
pij_EKF = [0.8 0.1 0.1;
            0.2 0.8 0;
            0.2 0 0.8];
        
 AEKF{1}=blkdiag(Ar,Ap);
 AEKF{2}=blkdiag(Ar,Ap);
 AEKF{3}=blkdiag(Ar,Ap);
 Q{1}=blkdiag(C_w_r,C_w_p);
 Q{2}=blkdiag(C_w_r,C_w_p);
 Q{3}=blkdiag(C_w_r,C_w_p);
R{1}= C_v;
R{2}=C_v;
R{3}=C_v;
 U{1}= [0 0 0 0 0 0 0]';
 U{2}=[0 0 0 0 pi/40 0 0]';
 U{3}=[0 0 0 0 pi/20 0 0]';
 ind{1}=[1 2 3 4 5 6 7];
 ind{2}=[1 2 3 4 5 6 7];
 ind{3}=[1 2 3 4 5 6 7];
 
% parameters for SOEKF
sgh1 = 1/4;
sgh2 = 1/4;
C_w = blkdiag(C_w_r, C_w_p);
Ax = blkdiag(Ar,Ap);

hat_x_SOEKF = hat_x0;
Cx_SOEKF = C_x0;




% parameters for Random Matrix
alpha = 50;
alphaLan = 50;
tau = 20;
delta = 20;

T = 10;
const_z = 1/4;
hat_x_RMM = hat_r0;
hat_X_RMM = get_random_matrix_state(hat_p0);
Cx_RMM = C_r0;

hat_x_RMMLan = hat_r0;
hat_X_RMMLan = get_random_matrix_state(hat_p0);
Cx_RMMLan = C_r0;
A = eye(2)/(delta)^(.5);
figure;

hold on

for t = 2:time_steps
    N = poissrnd(possion_lambda);
    while N == 0
        N = poissrnd(possion_lambda);
    end
    disp(['time step:' num2str(t) ', ' num2str(N) ' measurements']);
    
    %% ------------------get measurements------------------------------------
    gt_cur_par = gt_par(:,t);
    gt_velo = H_velo*gt_kin(:,t);
    gt_rot = [cos(gt_cur_par(3)), -sin(gt_cur_par(3)); sin(gt_cur_par(3)), cos(gt_cur_par(3))];
    gt_len = gt_cur_par(4:5);
    y = zeros(2,N);
    for n = 1:N
        h_noise(n,:) = -1 + 2.*rand(1,2);
        while norm(h_noise(n,:)) > 1
            h_noise(n,:) = -1 + 2.*rand(1,2);
        end
        y(:,n) = H*gt_kin(:,t) + gt_rot*diag(gt_len)*h_noise(n,:)'+ mvnrnd([0 0], C_v, 1)';
    end
    
    
    [xEKF,Cx_EKF,mu_ip,x,P] = imm_MEM_filter(xEKF,Cx_EKF,MU_EKF,pij_EKF,ind,7,AEKF,U,Q,y,R);
MU(:,t)=mu_ip';
    
    if mod(t,3)==2
        meas_points=plot( y(1,:)/1000, y(2,:)/1000, '.k','lineWidth',0.5);
        
        hold on
        


        gt_plot = plot_extent([gt_cur_par(1:2)/1000; gt_cur_par(3);gt_cur_par(4:5)/1000], '-','k',1);
        axis equal
        
%         est_plot_rmm = plot_extent([rmm_par(1:2)/1000;rmm_par(3);rmm_par(4:5)/1000],'-','g',1);
%         est_plot_rmmLan = plot_extent([rmmLan_par(1:2)/1000;rmmLan_par(3);rmmLan_par(4:5)/1000],'-','b',1);
        est_plot_batchekf = plot_extent([x(1:2)/1000;x(5);x(6:7)/1000],'-', 'm', 1);
        %    text(rmm_par(1)/1000,rmm_par(2)/1000,num2str(t))
    end
    
end