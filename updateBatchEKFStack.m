function [ x, C,lambda ] = updateBatchEKFStack(x, C, y, C_v)

C_h = diag([.25 .25]);
r_hat = x(1:4);
p_hat = x(5:7);
C_r = C(1:4,1:4);
C_p = C(5:7,5:7);

n=size(y,2);

cur_C_h = 1/n*C_h;
cur_C_v = 1/n*C_v;
H = [1 0 0 0;0 1 0 0];
[S_hat,M_hat] = getMS(p_hat,C_h);
% moments of kinematic state
mean_y = mean(y,2);
E_y = H*r_hat;
C_ry = C_r*H';
C_yy = H*C_r*H' +  S_hat*cur_C_h*S_hat'+cur_C_v;
lambda1 = mvnpdf(mean_y,E_y,C_yy);
% kinematic state update
r_hat = r_hat + C_ry*(C_yy)^(-1)*(mean_y-E_y);

C_r = C_r - C_ry*(C_yy)^(-1)*C_ry';
C_r = (C_r+C_r')/2;

%% pseudo measurement
% for i = 1:size(y,2)
%      [S_hat,M_hat] = getMS(p_hat,C_h);
% y_shift = y - repmat(E_y,1,size(y,2)); % shift the measuremet to match central moments
%y_shift = y(:,i)-E_y;
% Take the 2nd kronecker product of shifted measurements
% and delete the duplicated element we get the pseudo measurement
sum_Y = 0;
  for i = 1:n
%   Y(:,i) = [eye(2),zeros(2,2);0 0 0 1]*kron(y_shift(:,i),y_shift(:,i)); 
% sum_Y = sum_Y + (y(:,i)-mean_y)*(y(:,i)-mean_y)';
 sum_Y = sum_Y + (y(:,i)-mean_y)*(y(:,i)-mean_y)';
  end
cov_y = 1/n*sum_Y;
% y_shift = mean_y-E_y;
%  Y= ([eye(2),zeros(2,2);0 0 0 1]*kron(y_shift,y_shift));
% mean_Y = mean(Y,2);
% cov_y = cov(y');
Y = [cov_y(1,1);cov_y(1,2);cov_y(2,2)];
C_yy = H*C_r*H' +  S_hat*C_h*S_hat'+C_v;
sgm11 =  C_yy(1,1);
sgm12 = C_yy(1,2);
sgm22 = C_yy(2,2);
E_Y =  [sgm11; sgm12; sgm22];
% C_YY = [3*sgm11^2, 3*sgm11*sgm12,sgm11*sgm22 + 2*sgm12^2 ;...
%         3*sgm11*sgm12, sgm11*sgm22 + 2*sgm12^2, 3*sgm22*sgm12;
%        sgm11*sgm22 + 2*sgm12^2, 3*sgm22*sgm12,3*sgm22^2];
 C_YY = [3*sgm11^2, 3*sgm11*sgm12,sgm11*sgm22 + 2*sgm12^2 ;...
         3*sgm11*sgm12, sgm11*sgm22 + 2*sgm12^2, 3*sgm22*sgm12;
        sgm11*sgm22 + 2*sgm12^2, 3*sgm22*sgm12,3*sgm22^2];
%  C_YY = cov(Y');

    
C_pY =  C_p*M_hat';
%pseudo

lambda2 = get_cov_likelihood(sum_Y,p_hat,C_v,n);

lambda = lambda1*lambda2;

% shape variable update

p_hat = p_hat + C_pY*(C_YY)^(-1)*(Y-E_Y);
C_p = C_p - C_pY*(C_YY)^(-1)*C_pY';

%  end
C_p = (C_p + C_p')/2;

x = [r_hat;p_hat];
C = blkdiag(C_r,C_p);






end

function [S, M] = getMS(p,C_h)
a = p(1);
l1 = p(2);
l2 = p(3);
S = [cos(a) -sin(a); sin(a) cos(a)]*diag([l1 l2]);
M = [-sin(2*a), (cos(a))^2, (sin(a))^2  ;...
      cos(2*a), sin(2*a),   -sin(2*a);...     
      sin(2*a), (sin(a))^2, (cos(a))^2]*...
     diag([l1^2*C_h(1,1)-l2^2*C_h(2,2),2*l1*C_h(1,1),2*l2*C_h(2,2)]);
end