close all
clc
clear
dbstop warning
set(0,'defaulttextinterpreter','latex')

%% parameters
possion_lambda = 20;
H = [1 0 0 0; 0 1 0 0];
R = diag([50^2,50^2]);


%% generate ground truth
[gt_kin,gt_par, time_steps, delta_t] = get_ground_truth;
gt_X = get_random_matrix_state(gt_par(3:5));
%% setting prior
xLan = [100,100,5,-8]';
shape = [-pi/3,200,90]';
XLan = get_random_matrix_state(shape);
PLan = diag([100,100, 16,16]);
Q = blkdiag(100*eye(2),16*eye(2));

xFeldmann = [100,100,5,-8]';
XFeldmann = get_random_matrix_state(shape);
PFeldmann = diag([100,100, 16,16]);


alphaFeldmann = 50;
alphaLan = 50;
tau = 50;
delta = 50;

T = 10;
z = 1/4;

F = [eye(2),delta_t*eye(2); zeros(2,2),eye(2)];
A = eye(2)/(delta)^(.5);

figure;

hold on

for t = 1:time_steps
    N = poissrnd(possion_lambda);
    while N == 0
        N = poissrnd(possion_lambda);
    end
    disp(['time step:' num2str(t) ', ' num2str(N) ' measurements']);
    
    %% ------------------get measurements------------------------------------
    gt_cur_par = gt_par(:,t);
    gt_X = get_random_matrix_state(gt_par(3:5,t));
    y = mvnrnd(H*gt_kin(:,t), gt_X, N)';
    
    %% update
    meas_mean = mean(y,2);
    meas_spread = (N - 1) * cov(y');
    
    [xLan, XLan, PLan, alphaLan] = updateRMMLan(xLan, XLan, ...
        PLan, alphaLan,meas_mean, meas_spread, R,N,H,z);
    [~, lenLan,angLan] = get_random_matrix_ellipse(XLan);
    rmmLan_par = [H*xLan; angLan;lenLan];
    
    
  
     [xFeldmann, XFeldmann, PFeldmann, alphaFeldmann] = updateRMMFeldmann(xFeldmann, XFeldmann, ...
         PFeldmann, alphaFeldmann,meas_mean, meas_spread, R,N,H,z);
     [~, lenFeldmann,angFeldmann] = get_random_matrix_ellipse(XFeldmann);
     rmmFeldmann_par = [H*xFeldmann; angFeldmann;lenFeldmann];
    
    %% visulization udpated shapes
    if mod(t,3)==1
        meas_points=plot( y(1,:)/1000, y(2,:)/1000, '.k','lineWidth',0.5);
        hold on
        
        gt_plot = plot_extent([gt_cur_par(1:2)/1000; gt_cur_par(3);gt_cur_par(4:5)/1000], '-','k',1);
        axis equal
        
        est_plot_rmmLan = plot_extent([rmmLan_par(1:2)/1000;rmmLan_par(3);rmmLan_par(4:5)/1000],'-','b',1);
        est_plot_rmmFeldmann = plot_extent([rmmFeldmann_par(1:2)/1000;rmmFeldmann_par(3);rmmFeldmann_par(4:5)/1000],'-','g',1);
    end
    
    %%  predict
    [xLan,XLan,PLan, alphaLan] = predictRMMLan(xLan, XLan, PLan, alphaLan,F,Q,A,delta);
     [xFeldmann,XFeldmann,PFeldmann, alphaFeldmann] = predictRMMFeldmann(xFeldmann, XFeldmann, PFeldmann, alphaFeldmann,F,Q,T,tau);
    
end

legend([meas_points,gt_plot,est_plot_rmmLan],...
    {'measurement','ground truth','Lan'})
box on
grid on
ylim([-4500 1200]/1000)
xlim([-200 10200]/1000)


