function [x_update, X_update,P_update, v_update] = updateRMMLan(....
    x, X, P, v,z,barZ,R,n_k,H,const_scale)

d = numel(H);
v_update = v+n_k;
barX = X/(v-2*d-2);


 B = sqrtm(const_scale*barX+R)*(barX)^(-.5);
% I found following line would  give reasonable extension
%   B = sqrtm(.25*X+R)*(barX)^(-.5);
 S = H*P*H'+det(B)^(2/d)/n_k;
 
K = P*H'*S^(-1);
G = z-kron(H,eye(2))*x;

x_update = x+ kron(K,eye(2))*G;
P_update = P-K*S*K';



N = S^-1*(G*G');
X_update = X+N+B^(-1)*barZ*(B^(-1))';


end