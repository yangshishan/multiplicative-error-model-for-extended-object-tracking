close all
clc
clear
dbstop warning
set(0,'defaulttextinterpreter','latex')

%% parameters
possion_lambda = 20;


%% generate ground truth
[gt_kin,gt_par, time_steps, delta_t] = get_ground_truth;
% ellipse parameterized as [center1, center2, orientation, length1, length2]
gt_X = get_random_matrix_state(gt_par(3:5));
%% setting prior
T = 10;
H = [1 0];
R = diag([50^2,50^2]);
P = diag([100,16]);
Q = blkdiag(100,1);
x = [100,5, 100,-8]';
F = [1 T; 0 1];
shape = [-pi/3,200,90]';
X = get_random_matrix_state(shape);

HFeldmann = [1 0 0 0; 0 1 0 0];
PFeldmann = diag([100,100, 16,16]);
QFeldmann = blkdiag(100,100,1,1);
xFeldmann = [100 100 5 -8]';
XFeldmann = X;
FFeldmann = [eye(2),delta_t*eye(2); zeros(2,2),eye(2)];
vFeldmann = 50;
v = 50;
tau = 50;
delta = 50;


const_scale = 1/4;


A = eye(2)/(delta)^(.5);

figure;
hold on

for t = 1:time_steps
    N = poissrnd(possion_lambda);
    while N == 0
        N = poissrnd(possion_lambda);
    end
    disp(['time step:' num2str(t) ', ' num2str(N) ' measurements']);
    
    %% ------------------get measurements------------------------------------
    gt_cur_par = gt_par(:,t);
    gt_X = get_random_matrix_state(gt_par(3:5,t));
    z = mvnrnd(gt_kin(1:2,t), gt_X, N)';
    
    %% update
    barz = mean(z,2);
    barZ = (N - 1) * cov(z');
    
    [x, X, P, v] = updateRMMLan(x, X,P, v,barz, barZ, R,N,H,const_scale);
    [~, len,ang] = get_random_matrix_ellipse(X);
    center = kron(H,eye(2))*x;
    
    % get the orientationa and length for plotting
    Lan_par = [center; ang;len];
    velo(:,t) = kron([0 1], eye(2))*x;
    rmse_velo(t) = norm(gt_kin(3:4,t) - velo(:,t));
    
    
    
    
    
    [xFeldmann, XFeldmann, PFeldmann, vFeldmann] = updateRMMFeldmann(xFeldmann, XFeldmann, ...
        PFeldmann, vFeldmann,barz, barZ, R,N,HFeldmann,const_scale);
    [~, lenFeldmann,angFeldmann] = get_random_matrix_ellipse(XFeldmann);
    % get the orientationa and length for plotting
    Feldmann_par = [HFeldmann*xFeldmann; angFeldmann;lenFeldmann];
    veloFeldmann(:,t) = [0 0 1 0; 0 0 0 1]*xFeldmann;
    rmse_veloFeldmann(t) = norm(gt_kin(3:4,t) - veloFeldmann(:,t));
    
    %% visulization udpated shapes
    if mod(t,3)==1
        meas_points=plot( z(1,:), z(2,:), '.k','lineWidth',0.5);
        hold on
        
        gt_plot = plot_extent([gt_cur_par(1:2); gt_cur_par(3);gt_cur_par(4:5)], '-','k',1);
        axis equal
        
        est_plot_Lan = plot_extent([Lan_par(1:2);Lan_par(3);Lan_par(4:5)],'-','b',1);
        est_plot_Feldmann = plot_extent([Feldmann_par(1:2);Feldmann_par(3);Feldmann_par(4:5)],'-','g',1);
    end
    
    %%  predict
    [x,X,P, v] = predictRMMLan(x, X, P, v,F,Q,A,delta);
    [xFeldmann,XFeldmann,PFeldmann, vFeldmann] = predictRMMFeldmann(xFeldmann, XFeldmann, PFeldmann, vFeldmann,FFeldmann,QFeldmann,T,tau);
    
end

legend([meas_points,gt_plot,est_plot_Lan,est_plot_Feldmann], {'measurement','ground truth','Lan','Feldmann'})
box on
grid on
ylim([-4500 1200])
xlim([-200 10200])
figure
subplot(1,3,1)
plot(1:time_steps,mean(rmse_velo,1),'-b',1:time_steps,mean(rmse_veloFeldmann,1),'-g')
title('RMSE of velocity')
legend('Lan','Feldmann')
subplot(1,3,2)
plot(1:time_steps, gt_kin(3,:),'k-',1:time_steps,velo(1,:),'b-',1:time_steps,veloFeldmann(1,:),'g-')
title('velocity for x dimension ')
legend('gt','Lan','Feldmann')
subplot(1,3,3)
plot(1:time_steps, gt_kin(4,:),'k-',1:time_steps,velo(2,:),'b-',1:time_steps,veloFeldmann(2,:),'g-')
title('veolocity for y dimension')
legend('gt','estimation','Feldmann')

