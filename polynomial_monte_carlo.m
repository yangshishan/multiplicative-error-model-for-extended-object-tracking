function [ x_est,x_cov ] = polynomial_monte_carlo(x_est,x_cov,meas,meas_noise,Ch)
nr_samples = 10000;
x_samples = mvnrnd(x_est, x_cov,nr_samples)';


v_samples = mvnrnd([0 0], meas_noise, nr_samples)';
h_samples = mvnrnd([0 0], Ch,nr_samples)';
[x_dim, nr_samples] = size(x_samples);
center_dim = size(meas,1);


for s = 1:nr_samples
    
    x = x_samples(:,s);
    h = h_samples(:,s);
    v = v_samples(:,s);
    c =x(1:center_dim);
    
%     
%     if strcmp(ellipse_par,'ab')
%         a =x(center_dim +1 : center_dim + 2);
%         b =x(center_dim +3 : center_dim + 4);
%         xy_samples(:,s) = [x; a'*b;  meas - (h(1)*a + h(2)*b + c + v);...
%             (meas).^2-(c + h(1)*a + h(2)*b +v).^2;...
%             (meas(1)-(c(1) + h(1)*a(1) + h(2)*b(1) +v(1)))*(meas(2)-(c(2) + h(1)*a(2) + h(2)*b(2) +v(2)));];
%     elseif strcmp(ellipse_par,'el')
%         h1=h(1);
%         h2=h(2);
%         v1=v(1);
%         v2=v(2);
%         c1=c(1);
%         c2=c(2);
%         e = x(center_dim +1 : center_dim + 2);
%         e1=e(1);
%         e2=e(2);
%         l = x(center_dim +3 : center_dim + 4);
%         l1=l(1);
%         l2=l(2);
%         
%         xy_samples(:,s) = [x; e1^2 + e2^2;...
%             (c1 + h1*l1*e1 - h2*l2*e2 + v1);...
%             (c2 + h1*l1*e2 + h2*l2*e1 + v2);...
%             (c1 + h1*l1*e1 - h2*l2*e2 + v1)^2;...
%             (c2 + h1*l1*e2 + h2*l2*e1 + v2)^2;...
%             (c1 + h1*l1*e1 - h2*l2*e2 + v1)*(c2 + h1*l1*e2 + h2*l2*e1 + v2)];
%     elseif strcmp(ellipse_par,'alpha')
        h1=h(1);
        h2=h(2);
        v1=v(1);
        v2=v(2);
        c1=c(1);
        c2=c(2);
        w = x(center_dim +1 );
        l = x(center_dim +2 : center_dim + 3);
        l1=l(1);
        l2=l(2);
        
        xy_samples(:,s) = [x; ...
            (c1 + h1*l1*cos(w) - h2*l2*sin(w) + v1);...
            (c2 + h1*l1*sin(w) + h2*l2*cos(w) + v2);...
            (c1 + h1*l1*cos(w) - h2*l2*sin(w) + v1)^2;...
            (c2 + h1*l1*sin(w) + h2*l2*cos(w) + v2)^2;...
            (c1 + h1*l1*cos(w) - h2*l2*sin(w) + v1)*(c2 + h1*l1*sin(w) + h2*l2*cos(w) + v2)];
%     else
%         error('unknown ellipse parameterization')
%     end
    
end


C = cov(xy_samples');


mean_xy = mean(xy_samples,2);
C_xy = C(1:x_dim, x_dim + 1:end);
C_yy = C(x_dim+1:end, x_dim+1:end);
% if strcmp(ellipse_par,'ab')
%     x_est = x_est - C_xy * C_yy^-1 * mean_xy(x_dim + 1:end);
% elseif strcmp(ellipse_par,'el')
%     x_est = x_est + C_xy * C_yy^-1 *([1;meas(1);meas(2);meas(1)^2;meas(2)^2;...
%         meas(1)*meas(2)]- mean_xy(x_dim + 1:end));
% elseif strcmp(ellipse_par,'alpha')
    x_est = x_est + C_xy * C_yy^-1 *([meas(1);meas(2);meas(1)^2;meas(2)^2;...
    meas(1)*meas(2)]- mean_xy(x_dim + 1:end));
%     x_est = x_est + C_xy * C_yy^-1 *([meas(1);meas(2)]- mean_xy(x_dim + 1:end));
% end




x_cov = x_cov - C_xy * C_yy^-1*C_xy';
x_cov = (x_cov+x_cov')/2;
end

