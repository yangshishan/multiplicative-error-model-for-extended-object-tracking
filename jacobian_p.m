syms a l1 l2 sgm1 sgm2
S1 = [l1*cos(a) -l2*sin(a)];
S2 = [l1*sin(a) l2*cos(a)];
S1d=[-l1*sin(a) cos(a) 0;
    -l2*cos(a) 0 -sin(a)];
S2d=[l1*cos(a) sin(a) 0;
    -l2*sin(a) 0 cos(a)];
Ch = diag([sgm1 sgm2]);
jp = [2*S1*Ch*S1d;
    2*S2*Ch*S2d;
    S1*Ch*S2d+S2*Ch*S1d];