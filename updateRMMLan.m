function [x_update, X_update,P_update, alpha_update] = updateRMMLan(....
    x, X, P, alpha,y,y_cov,R,n_k,H)

d = 2;
alpha_update = alpha+n_k;
barX = X/(alpha-2*d-2);


 B = sqrtm(.25*X+R)*(barX)^(-.5);
  S = H*P*H'+det(B)^(2/d)*barX/n_k;
%   B = sqrtm(.25*X+R)*(X)^(-.5);
%   S = H*P*H'+det(B)^(2/d)/n_k;
K = P*H'*S^(-1);
G = y-H*x;

x_update = x+ K*G;
P_update = P-K*S*K';



N = S^-1*(G*G');
X_update = X+N+B^(-1)*y_cov*(B^(-1))';




% 
% d = 2;
% alpha_update = alpha+n_k;
% barX = X/(alpha-2*d-2);
% 
% 
%  B = sqrtm(.25*X+R)*(barX)^(-.5);
%   S = H*P*H'+det(B)^(2/d)/n_k;
%  
% K = P*H'*S^(-1);
% G = y-kron(H,eye(2))*x;
% 
% x_update = x+ kron(K,eye(2))*G;
% P_update = P-K*S*K';
% 
% 
% 
% N = S^-1*(G*G');
% X_update = X+N+B^(-1)*y_cov*(B^(-1))';


end