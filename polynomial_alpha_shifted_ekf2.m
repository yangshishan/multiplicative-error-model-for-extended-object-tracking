function [ X_est, X_cov ] = polynomial_alpha_shifted_ekf2(X_est, X_cov,meas,f_func_g,f_jacobian,f_hessian)
% 2nd order EKF
% z = [c1, c2, a1, a2, b1, b2, h1,h2,v1,v2,0,y1,y2,y1^2,y2^2,y1*y2]
dim_state = 5;
syms m1 w l1 m2 l2 h1 h2 v1 v2

cov_zz = zeros(dim_state,dim_state);

shifted = X_est(1:2);
val_subs = [0; 0; X_est(3:dim_state);0;0;0;0];
meas = meas-shifted;
 X_est(1:2) = [0;0]; 
 

subs_func_g = f_func_g(val_subs');
subs_jacobian = f_jacobian(val_subs');
subs_hessian = f_hessian(val_subs');


for i = 1:dim_state
    for j = 1:dim_state
        tic
        cov_zz(i,j) = subs_jacobian(i,:)*X_cov*subs_jacobian(j,:)'+ (1/2)*trace(subs_hessian(:,:,i)*X_cov*subs_hessian(:,:,j)*X_cov);
        
    end
end

mean_z = zeros(1,dim_state);
for i = 1:dim_state
    mean_z(i) = subs_func_g(i) + (1/2)*trace(subs_hessian(:,:,i)*X_cov);
end

cov_Xz = X_cov*subs_jacobian';

X_est = double(X_est + cov_Xz*cov_zz^-1 *([meas(1);meas(2);meas(1)^2;meas(2)^2;...
    meas(1)*meas(2)]- mean_z'));

 X_est(1:2) = X_est(1:2) + shifted;

X_cov = double(X_cov - cov_Xz*cov_zz^-1*cov_Xz');

end





