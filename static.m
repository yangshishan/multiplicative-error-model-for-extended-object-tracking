% close all
% close all
clc
clear all
%  rng default
% load('s.mat')
% rng(s)
gt = [0 0 sqrt(3)/2 2 9]';

% when cv is very small, batch method doesnt work very well
Cv = 1*diag([2 8]);
Ch = diag([1/4,1/4]);

nrMeas = 100;

rotMatrix = @(alpha) [cos(alpha) -sin(alpha); sin(alpha) cos(alpha)];
x0 = [1 1 0 1 12]';
%  x0=gt;
Cx0 = diag([1 1 1 1 4]);

x0_ekf = x0;
Cx0_ekf = Cx0;
x0_batch = x0;
Cx0_batch = Cx0;
x0ekf2 = x0;
Cx0ekf2 = Cx0;

alpha = 5;
alphaLan = 500;
tau = 20;
delta = 20;

T = 10;
const_z = 1/4;
hat_x_RMM = x0(1:2);
hat_X_RMM = get_random_matrix_state(x0(3:5));
Cx_RMM = Cx0(1:2,1:2);

hat_x_RMMLan = x0(1:2);
hat_X_RMMLan = get_random_matrix_state(x0(3:5));
Cx_RMMLan = Cx0(1:2,1:2);

[f_func_g,f_jacobian,f_hessian] = get_jacobian_hessian_ekf2('alpha','static',1/4,1/4);
% [Jp, Hp] = get_jacobian_hessian_ekf;
% h = mvnrnd([0 0],Ch,nrMeas)';

gtRot = rotMatrix(gt(3));
for n = 1:nrMeas
    h(n,:) = -1 + 2.*rand(1,2);
    while norm(h(n,:)) > 1
        h(n,:) = -1 + 2.*rand(1,2);
    end
    y(:,n) = gt(1:2) + gtRot*diag(gt(4:5))*h(n,:)'+ mvnrnd([0 0], Cv, 1)';
end

figure
hold on
plot_extent(gt,'-','k',2);



steps=1;



for i = 1:steps:nrMeas
    
    plot(y(1,i:i+steps-1),y(2,i:i+steps-1),'r.');
    
    [hat_x_RMM, hat_X_RMM, C_x_RMM, alpha_update]= updateRMM(hat_x_RMM, hat_X_RMM, Cx_RMM, alpha,mean(y,2), ...
    (nrMeas - 1) * cov(y(:,i:i+steps-1)'), Cv,nrMeas,eye(2),const_z);

[~, len_RMM,ang_RMM] = get_random_matrix_ellipse(hat_X_RMM);
rmm_par = [hat_x_RMM; ang_RMM;len_RMM];

plotrmm=plot_extent(rmm_par,'-','r',2);

    
    [hat_r_batch, Cr_batch,hat_p_batch, Cp_batch ] = updateBatchEKFmc...
        (eye(2),x0_batch(1:2), Cx0_batch(1:2,1:2), x0_batch(3:5), Cx0_batch(3:5,3:5), y(:,i:i+steps-1), Cv, Ch);
    batchekf=plot_extent([hat_r_batch;hat_p_batch],'-','g',2);
    x0_batch = [hat_r_batch; hat_p_batch];
    Cx0_batch = blkdiag(Cr_batch, Cp_batch)+0.01*eye(5);
    pause(0.01)
    for j = i:i+steps-1
%         
%         
        [ x0,Cx0 ] = polynomial_monte_carlo(x0,Cx0,y(:,j),Cv,Ch);
        plotmc=plot_extent(x0,'-','c',2);
%         
%         %%
%         CX0ekf2 = blkdiag(Cx0ekf2,Ch,Cv);
%         X0ekf2 = [x0ekf2;0;0;0;0];
%         [X0ekf2, CX0ekf2 ] = polynomial_alpha_shifted_ekf2(X0ekf2,CX0ekf2 ,y(:,j),f_func_g,f_jacobian,f_hessian);
%         Cx0ekf2 = CX0ekf2(1:5,1:5)+ 0.01*eye(5);
%         x0ekf2 = X0ekf2(1:5);
%         plotekf2 = plot_extent(x0ekf2(1:5),'-','b',2);
%         
%         %% sequential ekf update
%         [hat_r_ekf, Cr_ekf, hat_p_ekf, Cp_ekf ] = updateEKF(eye(2),x0_ekf(1:2), Cx0_ekf(1:2,1:2),...
%             x0_ekf(3:5), Cx0_ekf(3:5,3:5), y(:,j), Cv, Ch);
%         x0_ekf = [hat_r_ekf; hat_p_ekf];
%         Cx0_ekf = blkdiag(Cr_ekf, Cp_ekf)+0.01*eye(5);
%         ekf = plot_extent(x0_ekf,'-','m',2);
%         
%         %%
         pause(0.01)
         if j<i+steps-1
             delete(plotmc)
%             delete(ekf)
%             delete(plotekf2)
         end
     end
    
    
    
    plot(y(1,i:i+steps-1),y(2,i:i+steps-1),'k.');
    
    pause(0.001)
    
    if i <= nrMeas-steps
%         delete(plotekf2)
%         delete(ekf)
        delete(batchekf)
        delete(plotrmm)
         delete(plotmc)
    end
end
