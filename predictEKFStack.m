function [x, C] = predictEKFStack(x, C,A,U, C_w)
  

x = A*x+U;
C= A*C*A'+C_w;

end