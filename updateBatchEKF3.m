function [ r_hat, C_r,p_hat, C_p ] = updateBatchEKF3(H,r_hat, C_r_o, p_hat, C_p, y, C_v, C_h)


    
n=size(y,2);

cur_C_h = 1/n*C_h;
cur_C_v = 1/n*C_v;
% 
[S_hat,M_hat] = getMS(p_hat,C_h);
% moments of kinematic state
mean_y = mean(y,2);
E_y = H*r_hat;
C_ry = C_r_o*H';
C_yy = H*C_r_o*H' + S_hat*cur_C_h*S_hat'+cur_C_v;

% kinematic state update
r_hat = r_hat + C_ry*(C_yy)^(-1)*(mean_y-E_y);

C_r = C_r_o - C_ry*(C_yy)^(-1)*C_ry';
C_r = (C_r+C_r')/2;

%% pseudo measurement
% for i = 1:size(y,2)
%      [S_hat,M_hat] = getMS(p_hat,C_h);
% y_shift = y - repmat(E_y,1,size(y,2)); % shift the measuremet to match central moments
%y_shift = y(:,i)-E_y;
% Take the 2nd kronecker product of shifted measurements
% and delete the duplicated element we get the pseudo measurement
sum_Y = 0;
  for i = 1:n
%   Y(:,i) = [eye(2),zeros(2,2);0 0 0 1]*kron(y_shift(:,i),y_shift(:,i)); 
%  sum_Y = sum_Y + (y(:,i)-E_y)*(y(:,i)-E_y)';
 sum_Y = sum_Y + (y(:,i)-mean_y)*(y(:,i)-mean_y)';
  end
cov_y = 1/n*sum_Y;
% y_shift = mean_y-E_y;
%  Y= ([eye(2),zeros(2,2);0 0 0 1]*kron(y_shift,y_shift));
% mean_Y = mean(Y,2);
% cov_y = cov(y');
C_YY = M_hat*C_p*M_hat';
C_pY = C_p*M_hat';
%pseudo
% shape variable update

p_hat = p_hat + C_pY*(C_YY)^(-1)*(Y-E_Y);
C_p = C_p - C_pY*(C_YY)^(-1)*C_pY';

%  end
C_p = (C_p + C_p')/2;
end

function [S1,S2, S1p,S2p] = getMS(p,C_h)
a = p(1);
l1 = p(2);
l2 = p(3);
S = [cos(a) -sin(a); sin(a) cos(a)]*diag([l1 l2]);
S1 = S(:,1);
S2 = S(:,2);

S1p = [-l1*sin(a) cos(a) 0;
    -l2*cos(a) 0 -sin(a)];
S2p = [l1*cos(a) sin(a) 0;
    -l2sin(a) 0 cos(a)];

end