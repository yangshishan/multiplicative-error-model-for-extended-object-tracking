function lambda = measRMMFeldmann_lhood(x,X,P,alpha,bary,barY,n_k,H,R)
d = size(X,1);

Yj = .25*X+R; 
deltaj = barY-(n_k-1)*Yj;

Vj =(alpha*trace(X)*X+(alpha+2)*X^2)/((alpha+1)*(alpha-2));

var_delta_Yj = (n_k-1)*(trace(Yj)*Yj+(Yj)^2);
var_deltaj = var_delta_Yj+0.25^2*(n_k-1)^2*Vj;

S = H*P*H'+(.25*X+R)/n_k;
S = (S+S')/2;
lambda1 = mvnpdf(bary,H*x, S);
lambda2 = det(2*pi*var_deltaj)^(-(d+1)/4)*exp(trace(-.5*deltaj*(var_deltaj)^-1*deltaj));

lambda = lambda1*lambda2;
end