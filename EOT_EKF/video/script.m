
files = dir([ '*.png']);

count = 1;
for i = 1:length(files)


old = files(i).name;
if mod(str2num(strrep(old,'.png','')),2) == 1
    delete(old);
    disp(['delete ' old])
else
    new = sprintf('%04d.jpg',count)
movefile(old,new);
count = count+1;
end

end 