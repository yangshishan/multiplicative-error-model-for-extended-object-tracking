In the following we derive the Jacobians of the pseudo-measurement equation \eqref{eq:pseudo_meas}.
The time index $k$ is omitted for compact expression. 
Let $\mat{S}_{1}$ and $\mat{S}_{2}$ denote the first and second row of matrix S, i.e.,
{\footnotesize
\begin{eqnarray}
\mat{S}_1&=\vect{l_1\cos{\alpha} & -l_2\sin{\alpha}}\\
\mat{S}_2&=\vect{l_1\sin{\alpha} & l_2\cos{\alpha}}\enspace.
\end{eqnarray}}
Similarly, $\mat{H}_1$ and $\mat{H}_2$ are refer to the first and second row of $\mat{H}$.
Accordingly, pseudo-measurement equation \eqref{eq:pseudo_meas} is rewritten as
{\footnotesize
\begin{equation}\label{eq:pseudo_meas_nok}
g(\rvec{r},\rvec{p})=\vect{\expect{\left(\mat{H}_1\rvec{r}+\mat{S}_1\rvec{h}+\rvec{v}_{1}-\bar{y}_1\right)^2}\\\expect{\left(\mat{H}_2\rvec{r}+\mat{S}_2\rvec{h}+\rvec{v}_{2}-\bar{y}_{1}\right)^2}\\\expect{\left(\mat{H}_1\rvec{r}+\mat{S}_1\rvec{h}+\rvec{v}_{1}-\bar{y}_1\right)\left(\mat{H}_2\rvec{r}+\mat{S}_2\rvec{h}+\rvec{v}_{2}-\bar{y}_2\right)}}
\end{equation}}
By linearizing $g(\rvec{r},\rvec{p})$ at $\hat{\rvec{r}}$ and $\hat{\rvec{p}}$, we have
\begin{equation}
g(\rvec{r},\rvec{p})\approx g(\hat{\rvec{r}},\hat{\rvec{p}}) + \left.\frac{\partial g}{\partial \rvec{r}}\right|_{\rvec{r}=\hat{\rvec{r}}_k}(\rvec{r}-\hat{\rvec{r}})+\left.\frac{\partial g}{\partial \rvec{p}}\right|_{\rvec{p}=\hat{\rvec{p}}}(\rvec{p}-\hat{\rvec{p}})
\end{equation}
First, we calculate the Jacobian matrix of kinematic state evaluated at $\hat{\vec{r}}$.
By applying the chain rule, we have $\frac{\partial g}{\partial \rvec{r}}$ equals
{\footnotesize
\begin{equation}
\vect{\expect{2\left(\mat{H}_1\rvec{r}+\mat{S}_1\rvec{h}+\rvec{v}_{1}-\bar{y}_1\right)\mat{H}_1}\\
											\expect{2\left(\mat{H}_2\rvec{r}+\mat{S}_2\rvec{h}+\rvec{v}_{2}-\bar{y}_{1}\right)\mat{H}_2}\\
											\expect{\left(\mat{H}_1\rvec{r}+\mat{S}_1\rvec{h}+\rvec{v}_{1}-\bar{y}_1\right)\mat{H}_2+\left(\mat{H}_2\rvec{r}+\mat{S}_2\rvec{h}+\rvec{v}_{2}-\bar{y}_{1}\right)\mat{H}_1}}
\end{equation}}
As both additive and multiplicative noises are zeros mean and independent to shape variables, we have
\begin{equation}\label{eq:kin_jacobian}
\frac{\partial g}{\partial \rvec{r}}=\vect{2\expect{\mat{H}_1\rvec{r}-\bar{y}_1}\mat{H}_1\\
											2\expect{\mat{H}_2\rvec{r}-\bar{y}_2}\mat{H}_2\\
											\expect{\mat{H}_1\rvec{r}-\bar{y}_1}\mat{H}_2+\expect{\mat{H}_2\rvec{r}-\bar{y}_{1}}\mat{H}_1}
\end{equation}
Evaluating \eqref{eq:kin_jacobian}  at $\hat{\rvec{r}}$, together with  $\bar{\rvec{y}}=\mat{H}\hat{\rvec{r}}$, we have
{\footnotesize
\begin{equation}
\left.\frac{\partial g}{\partial \rvec{r}}\right|_{\rvec{r}=\hat{\rvec{r}}_k}=\rvec{0}
\end{equation}}
Then, we calculate the Jacobian of shape variables. Again, by applying the chain rule,  $\frac{\partial g}{\partial \rvec{p}}$ equals
{\footnotesize
\begin{equation}
 \vect{\expect{2\left(\mat{H}_1\rvec{r}+\mat{S}_1\rvec{h}+\rvec{v}_{1}-\bar{y}_1\right)\rvec{h}^{\tp}\mat{S}^{'}_1}\\
											\expect{2\left(\mat{H}_2\rvec{r}+\mat{S}_2\rvec{h}+\rvec{v}_{2}-\bar{y}_{1}\right)\rvec{h}^{\tp}\mat{S}^{'}_2}\\
										\expect{2\left(\mat{H}_1\rvec{r}+\mat{S}_1\rvec{h}+\rvec{v}_{1}-\bar{y}_1\right)\rvec{h}^{\tp}\mat{S}^{'}_{2}+\left(\mat{H}_2\rvec{r}+\mat{S}_2\rvec{h}+\rvec{v}_{2}-\bar{y}_{1}\right)\rvec{h}^{\tp}\mat{S}^{'}_{1}}}
\end{equation}
}
with 
{\footnotesize
\begin{eqnarray}\notag
\mat{S}_1^{'}&=\frac{\partial \mat{S}_1}{\partial \rvec{p}}=& \vect{-l_1\sin{\alpha} &\cos{\alpha}&0\\-l_2\cos{\alpha}&0&-\sin{\alpha}}\\\notag
\mat{S}_2^{'}&=\frac{\partial \mat{S}_2}{\partial \rvec{p}}=&\vect{l_1\cos{\alpha}&\sin{\alpha}&0\\-l_2\sin{\alpha}&0&\cos{\alpha}}
\end{eqnarray}}
By exploiting the independences between shape and kinematic variables, noise terms and elliptical parameters, we have
{\footnotesize
\begin{equation}\label{eq:shape_jacobian}
\frac{\partial g}{\partial \rvec{p}}= \vect{\expect{2\mat{S}_1\rvec{h}\rvec{h}^{\tp}\mat{S}_1^{'}}\\
											\expect{2\mat{S}_2\rvec{h}\rvec{h}^{\tp}\mat{S}_2^{'}}\\
										\expect{\mat{S}_1\rvec{h}\rvec{h}^{\tp}\mat{S}_2^{'}+\mat{S}_2\rvec{h}\rvec{h}^{\tp}\mat{S}_1^{'}}}
\end{equation}
}
Evaluate \eqref{eq:shape_jacobian}, together with $\expect{\rvec{h}\rvec{h}^{\tp}}=\mat{C}^h$, we have 
{\footnotesize
\begin{equation}\label{eq:shape_jacobian}
\left.\frac{\partial g}{\partial \rvec{p}}\right|_{\rvec{p}_k=\hat{\rvec{p}}}= 
			\vect{2\hat{\mat{S}}_1\mat{C}^h\hat{\mat{S}}^{'}_1\\
					2\hat{\mat{S}}_2\mat{C}^h\hat{\mat{S}}^{'}_2\\
					\hat{\mat{S}}_1\mat{C}^h\hat{\mat{S}}^{'}_2+\hat{\mat{S}}_2\mat{C}^h\hat{\mat{S}}^{'}_1}\enspace.
\end{equation}
}
with the $\hat{(\cdot)}$ sign indicates the matrix $(\cdot)$ is evaluated at $\hat{\rvec{p}}$.
%{\small
%	\begin{align}
%	\label{eq:matrixM}
%		\begin{split}
%		\underbrace{\left.\frac{\partial \mathbf{y}_k}{\partial \rvec{p}_k}\right|_{\rvec{p}_k=\hat{\rvec{p}}_k}}_{:=\mat{M}}=&	\vect{ -\sin{2\hat{\alpha}_k} & \cos^2{\hat{\alpha}_k}& \sin^2{\hat{\alpha}_k}\\
%  						\sin{2\hat{\alpha}_k} & \sin^2{\hat{\alpha}_k} &\cos^2{\hat{\alpha}_k}\\
% 				 		\cos{2\hat{\alpha}_k} &  \sin{2\hat{\alpha}_k} & -\sin{2\hat{\alpha}_k}}\\
% 		&\cdot \vect{(\hat{l}_{k,1})^2\sigma_{h_1}-(\hat{l}_{k,2})^2\sigma_{h_2}&0&0\\
% 				 			0&2\hat{l}_{k,1}\sigma_{h_1}&0\\0&0& 2\hat{l}_{k,2}\sigma_{h_2}}
%		\end{split}
%	\end{align}}
%	\begin{equation}\label{eq:linearization}
%\mathbf{y} \approx g(\hat{\rvec{r}},\hat{\rvec{p}})+\mat{M}(\rvec{p}-\hat{\rvec{p}})
%\end{equation}